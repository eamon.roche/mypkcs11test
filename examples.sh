#!/bin/sh
clear
set -x
CLEAR_TEXT='The quick brown fox jumps over the lazy dog.'
#./mypkcs11test -library "$1" -token_label "$2" -pin "$3" -sha1 "$CLEAR_TEXT"
#./mypkcs11test -library $1 -token_label $2 -pin $3 -secretkey_label MYAESKEY-001 -genkey_aes32
#./mypkcs11test -library $1 -token_label $2 -pin $3 -secretkey_label MYAESKEY-002 -genkey_aes24
#./mypkcs11test -library $1 -token_label $2 -pin $3 -secretkey_label MYAESKEY-003 -genkey_aes16
#./mypkcs11test -library $1 -token_label $2 -pin $3 -secretkey_label MYSECRETKEY-001 -importkey 0x000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f
./mypkcs11test -library $1 -token_label $2 -pin $3 -listkeys
#./mypkcs11test -library $1 -token_label $2 -pin $3 -secretkey_label MYSECRETKEY-001 -hmac_sha256 "$CLEAR_TEXT"
#./mypkcs11test -library $1 -token_label $2 -pin $3 -secretkey_label MYAESKEY-001 -aes_cbc_pad_enc "$CLEAR_TEXT"
#./mypkcs11test -library $1 -token_label $2 -pin $3 -secretkey_label MYAESKEY-002 -aes_cbc_pad_enc "$CLEAR_TEXT"
#./mypkcs11test -library $1 -token_label $2 -pin $3 -secretkey_label MYAESKEY-003 -aes_cbc_pad_enc "$CLEAR_TEXT"
#./mypkcs11test -library $1 -token_label $2 -pin $3 -secretkey_label MYAESKEY-001 -delkey
#./mypkcs11test -library $1 -token_label $2 -pin $3 -secretkey_label MYAESKEY-002 -delkey
#./mypkcs11test -library $1 -token_label $2 -pin $3 -secretkey_label MYAESKEY-003 -delkey
#./mypkcs11test -library $1 -token_label $2 -pin $3 -secretkey_label MYSECRETKEY-001 -delkey
#./mypkcs11test -library $1 -token_label $2 -pin $3 -secretkey_label KEYTOOL-AES -hmac_sha256 "$CLEAR_TEXT"
#./mypkcs11test -library $1 -token_label $2 -pin $3 -secretkey_label KEYTOOL-AES -aes_cbc_pad_enc "$CLEAR_TEXT"
#./mypkcs11test -library $1 -token_label $2 -pin $3 -secretkey_label KEYTOOL-AES -aes_cbc_pad_enc 0x54686520717569636b2062726f776e20666f78206a756d7073206f76657220746865206c617a7920646f672e
#./mypkcs11test -library $1 -token_label $2 -pin $3 -secretkey_label KEYTOOL-AES -aes_cbc_pad_dec 0xbce46469e2f7ab6b7ea767bd3252529a843ba24e890e567ef600c4a6e1051ffce0b1424e2103017de5778b49dea6d819
./mypkcs11test -library $1 -token_label $2 -pin $3 -secretkey_label KEYTOOL-AES -aes_gcm_enc "$CLEAR_TEXT"
./mypkcs11test -library $1 -token_label $2 -pin $3 -secretkey_label KEYTOOL-AES -aes_gcm_dec 0x5ad4d0fec459eade6388cb47775bffb9b42c2e7342f40d5ff3bedcf5a08d7d22263ac6d04b88c12657ac16db078a3b1bd0265eae67b78ae8ef5efcc0
