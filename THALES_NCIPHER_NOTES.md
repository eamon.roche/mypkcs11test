Thales nCipher Notes
====================
## Configuring the Token
```
# umask 022

# # Configure the slot
# /opt/nfast/bin/ppmk --new DEV_SOFTCARD

# ./ckcheckinst 
PKCS#11 library interface version 2.01
                            flags 0
                   manufacturerID "nCipher Corp. Ltd               "
               libraryDescription "nCipher PKCS#11 2.19.1          "
           implementation version 2.19
         Loadsharing and Failover enabled

Slot  Status            Label
====  ======            =====
   0  Fixed token       "loadshared accelerator          "
   1  Soft token        "DEV_SOFTCARD                    "


No removable tokens present.
Please insert an operator card into at least one available slot and enter 'R' retry.
If you have not created an operator card,
 enter a fixed token slot number, 
 or 'E' to exit this program and create a card set before continuing.

Enter 'R'etry or 'E'xit: 
```

## Generate Secret Key using the nCipher generatekey utility:
### To Create an AES key:
```
# ./generatekey pkcs11
protect: Protected by? (softcard, module) [softcard] > 
recovery: Key recovery? (yes/no) [yes] > 
type: Key type? (DES3, DH, DSA, HMACSHA1, HMACSHA256, HMACSHA384, HMACSHA512,
                 RSA, DES2, AES, Rijndael) [RSA] > AES
size: Key size? (bits, 128-256) [] > 256
plainname: Key name? [] > GENKEY-AES
nvram: Blob in NVRAM (needs ACS)? (yes/no) [no] > 
key generation parameters:
 operation    Operation to perform       generate
 application  Application                pkcs11
 protect      Protected by               softcard
 softcard     Soft card to protect key   DEV_SOFTCARD
 recovery     Key recovery               yes
 verify       Verify security of key     yes
 type         Key type                   AES
 size         Key size                   256
 plainname    Key name                   GENKEY-AES
 nvram        Blob in NVRAM (needs ACS)  no
Please enter the pass phrase for softcard `DEV_SOFTCARD':

Please wait........
Key successfully generated.
Path to key: /opt/nfast/kmdata/local/key_pkcs11_uc03472e1426e958582707d390a17f90c4a08b9c22-25b7401b32efe4066c3914b28a0fabac4205ca30
```
### To Create a Generic Secret Key used for HMACSHA256:
```
# ./generatekey pksc11
ERROR: sorry, application pksc11 is not currently usable
[root@DEV-RHEL6 bin]# ./generatekey pkcs11
protect: Protected by? (softcard, module) [softcard] > 
recovery: Key recovery? (yes/no) [yes] > 
type: Key type? (DES3, DH, DSA, HMACSHA1, HMACSHA256, HMACSHA384, HMACSHA512,
                 RSA, DES2, AES, Rijndael) [RSA] > HMACSHA256
size: Key size? (bits, 80-2048) [] > 256
plainname: Key name? [] > GENKEY-HMACSHA256
nvram: Blob in NVRAM (needs ACS)? (yes/no) [no] > 
key generation parameters:
 operation    Operation to perform       generate
 application  Application                pkcs11
 protect      Protected by               softcard
 softcard     Soft card to protect key   DEV_SOFTCARD
 recovery     Key recovery               yes
 verify       Verify security of key     yes
 type         Key type                   HMACSHA256
 size         Key size                   256
 plainname    Key name                   GENKEY-HMACSHA256
 nvram        Blob in NVRAM (needs ACS)  no
Please enter the pass phrase for softcard `DEV_SOFTCARD':

Please wait........
Key successfully generated.
Path to key: /opt/nfast/kmdata/local/key_pkcs11_uc03472e1426e958582707d390a17f90c4a08b9c22-eb6a73305f3adcceae7ff0ff36cc5f17b8bf3a84```
```

### To import an AES key using the nCipherKM JCA/JCE CSP with the Java keytool:
```
# # Configure the nCipher Provider
# export JAVA_HOME=/usr/lib/jvm/java-1.8.0-oracle-1.8.0.131.x86_64
# cp /opt/nfast/java/classes/nCipherKM.jar $JAVA_HOME/jre/lib/ext
# chmod 644 $JAVA_HOME/jre/lib/ext/nCipherKM.jar

# # Update Hardserver configuration
# vi /opt/nfast/kmdata/config/config
# # ensure nonpriv_port=9000 and priv_port=9001

# # Restart the client
# /opt/nfast/sbin/init.d-ncipher restart

# ./nfkminfo -s
SoftCard summary - 1 softcards: 
 Operator logical token hash               name
 03472e1426e958582707d390a17f90c4a08b9c22  DEV_SOFTCARD

# echo 03472e1426e958582707d390a17f90c4a08b9c22  >  ncipher.keystore
# java -Dprotect=03472e1426e958582707d390a17f90c4a08b9c22 -Djava.security.properties=/usr/lib/jvm/java-1.8.0-oracle-1.8.0.131.x86_64/jre/lib/security/java.security sun.security.tools.keytool.Main -importkeystore -srcstoretype JCEKS -srckeystore myAesKeyStore.jks -srcalias MYAESKEY-001 -srcstorepass securekey -srckeypass securekey -providerClass com.ncipher.provider.km.nCipherKM -providername nCipherKM -deststoretype nCipher.sworld -destkeystore ncipher.keystore -destalias KEYTOOL-AES -deststorepass <password>
FAILED!!!  keytool error: java.io.IOException: Unable to load the keystore key key_jcecsp_13472e1426e958582707d390a17f90c4a08b9c22

# keytool -importkeystore -srcstoretype JCEKS -srckeystore myAesKeyStore.jks -srcalias MYAESKEY-001 -srcstorepass securekey -srckeypass securekey -providerClass sun.security.pkcs11.SunPKCS11 -providerArg /opt/nfast/toolkits/pkcs11/pkcs11.cfg -deststorepass <password> -destalias KEYTOOL-AES -deststoretype PKCS11
WORKS!!! But key is unuasable for HMACSHA256

# ./mypkcs11test -library /opt/nfast/toolkits/pkcs11/libcknfast.so -token_label DEV_SOFTCARD -pin <password> -listkeys
Key 00 : KEYTOOL-AES

# ./mypkcs11test -library /opt/nfast/toolkits/pkcs11/libcknfast.so -token_label DEV_SOFTCARD -pin <password> -secretkey_label KEYTOOL-AES -hmac_sha256 'The quick brown fox jumps over the lazy dog.'
panic: pkcs11: 0x68: CKR_KEY_FUNCTION_NOT_PERMITTED

goroutine 1 [running]:
main.hmacSha256Example(0xc420082018, 0x8cb, 0x45f, 0x7ffc512d4935, 0x2c)
        /home/dev/GoCode/src/gitlab.com/eamon.ca/mypkcs11test/client.go:159 +0xc3
main.main()
        /home/dev/GoCode/src/gitlab.com/eamon.ca/mypkcs11test/client.go:372 +0x14c2

# # Whereas it should be:
HMAC-SHA256 of "The quick brown fox jumps over the lazy dog." is 0x67a4d65b99323ae833c98f47e4a1b9dbeccc2b59557319c827e193dfeee181e8
```
