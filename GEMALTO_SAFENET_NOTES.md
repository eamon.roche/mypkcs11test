Gemalto SafeNet Notes
=====================
```
# sudo yum install kernel-devel -y
# sudo yum install glibc.i686

# # Configure the slot
# ./lunacm -c clientconfig deploy -server 10.0.10.50 -client 10.0.14.130 -partition myslot -password <password>
LunaCM v6.2.2-4. Copyright (c) 2006-2015 SafeNet, Inc.
Command:  clientconfig  deploy  -server  10.0.10.50  -client  10.0.14.130  -partition  myslot  -password  <password> 
Please wait...

Using username "admin".
Last login: Wed Jul  5 16:50:56 2017 from 10.0.14.130

Luna SA 6.2.2-5 Command Line Shell - Copyright (c) 2001-2016 SafeNet, Inc. All rights reserved.


Private Key created and written to: /usr/safenet/lunaclient/cert/client/10.0.14.130Key.pem
Certificate created and written to: /usr/safenet/lunaclient/cert/client/10.0.14.130.pem



New server 10.0.10.50 successfully added to server list.



The following Luna SA Slots/Partitions were found: 

Slot    Serial #                Label
====    ================        =====
   0           543455016        myslot


Command Result : No Error 
```
### To import an AES key using the Java keytool utility:
```
# tar xvzf gemaltoclient64.tar.gz
# cd 64
# sh ./install.sh all

# sudo yum install kernel-devel -y
# sudo yum install glibc.i686

# cd /usr/safenet/lunaclient/bin

# # To uninstall (doesn't remove configured slots
# sh uninstall.sh

# # Configure the slot
# ./lunacm -c clientconfig deploy -server 10.0.10.50 -client 10.0.14.130 -partition myslot -password <password>

# # Set up LunaProvider
# export JAVA_HOME=/usr/lib/jvm/java-1.8.0-oracle-1.8.0.131.x86_64
# sudo cp /usr/safenet/lunaclient/jsp/lib/LunaProvider.jar $JAVA_HOME/jre/lib/ext
# sudo cp /usr/safenet/lunaclient/jsp/lib/libLunaAPI.so $JAVA_HOME/jre/lib/ext
# chmod 755 $JAVA_HOME/jre/lib/ext/LunaProvider.jar
# chmod 755 $JAVA_HOME/jre/lib/ext/libLunaAPI.so
# vi $JAVA_HOME/jre/lib/security/java.security
# # Add line: security.provider.10=com.safenetinc.luna.provider.LunaProvider

# pwd
/usr/safenet/lunaclient/bin
# ./vtl listslots
Number of slots: 1

The following slots were found:

Slot Description          Label                            Serial #         Status      
==== ==================== ================================ ================ ============
   0 LunaNet Slot         myvmeslot                        543455016        Present     

# echo "slot:0" > luna.keystore
# sudo keytool -importkeystore -srcstoretype JCEKS -srckeystore myAesKeyStore.jks -srcalias MYAESKEY-001 -srcstorepass securekey -srckeypass securekey -providerClass com.safenetinc.luna.provider.LunaProvider -deststorepass <password> -destalias KEYTOOL-AES -deststoretype Luna -destkeystore luna.keystore 

# ./mypkcs11test -library /usr/safenet/lunaclient/lib/libCryptoki2_64.so -token_label myvmeslot -pin <password> -listkeys
Key 00 : KEYTOOL-AES

# ./mypkcs11test -library /usr/safenet/lunaclient/lib/libCryptoki2_64.so -token_label myvmeslot -pin <password> -secretkey_label KEYTOOL-AES -hmac_sha256 'The quick brown fox jumps over the lazy dog.'
HMAC-SHA256 of "The quick brown fox jumps over the lazy dog." is 0x67a4d65b99323ae833c98f47e4a1b9dbeccc2b59557319c827e193dfeee181e8
```
