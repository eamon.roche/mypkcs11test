MyPKCS11Test
============
A simple test project that uses github.com/miekg/pkcs11 against SoftHSMv2 on Ubuntu, OSX, and Centos.
- https://github.com/miekg/pkcs11
- https://www.opendnssec.org/softhsm/

(see notes for testing done on RedHat (Centos) against Thales nCipher and Gemalo SafeNet)

## PKCS11 Documentation
* http://docs.oasis-open.org/pkcs11/pkcs11-base/v2.40/os/pkcs11-base-v2.40-os.html

## SoftHSMv2

### Installation

For Windows, Ubuntu, and OSX
- https://github.com/opendnssec/SoftHSMv2.git get tag 2.4.0 (i.e. `git checkout tags/2.4.0`)
- to build see OSX-NOTES.md, but this is what worked for me:

```
sh ./autogen.sh
./configure -with-openssl=/usr/local/opt/openssl --with-crypto-backend=openssl
make
sudo make install
```

For RedHat or Centos 6
- see CENTOS_NOTES.md

### Configuration Example

```
softhsm2-util --init-token --slot 0 --label "Token-001" --pin 1234 --so-pin 5678
```

### Importing a Java Keystore
First create a PKCS11 configuration file (i.e. /etc/softhsm/pkcs11.cfg) with the following or similar content:
```
name=SoftHSM
library=/usr/lib/softhsm/libsofthsm2.so
slotListIndex=0

attributes(*,*,*) = {
CKA_TOKEN = true
}
attributes(*,CKO_SECRET_KEY,*) = {
   CKA_PRIVATE=true
   CKA_SENSITIVE=true
   CKA_ENCRYPT=true
   CKA_DECRYPT=true
   CKA_WRAP=true
   CKA_UNWRAP=true
}
attributes(*,CKO_PRIVATE_KEY,*) = {
   CKA_LABEL=true
   CKA_PRIVATE=true
   CKA_DECRYPT=true
   CKA_SIGN=true
   CKA_UNWRAP=true
}
attributes(*,CKO_PUBLIC_KEY,*) = {
   CKA_LABEL=true
   CKA_ENCRYPT=true
   CKA_VERIFY=true
   CKA_WRAP=true
}
```
Then, you can use Java keytool to import a JCEKS keystore:
```
$ keytool -importkeystore -srcstoretype JCEKS -srckeystore myAesKeyStore.jks -srcalias MYAESKEY-001 -srcstorepass securekey -srckeypass securekey -providerClass sun.security.pkcs11.SunPKCS11 -providerArg /etc/softhsm/pkcs11.cfg -deststorepass 1234 -destalias KEYTOOL-AES -deststoretype PKCS11
```

## Usage Examples 

```
++ ./mypkcs11test -library /usr/lib/softhsm/libsofthsm2.so -token_label Token-001 -pin 1234 -sha1 'The quick brown fox jumps over the lazy dog.'
SHA1 of "The quick brown fox jumps over the lazy dog." is 0x408d94384216f890ff7a0c3528e8bed1e0b01621

++ ./mypkcs11test -library /usr/lib/softhsm/libsofthsm2.so -token_label Token-001 -pin 1234 -secretkey_label MYAESKEY-001 -genkey_aes32
Secret Key "MYAESKEY-001" generated.

++ ./mypkcs11test -library /usr/lib/softhsm/libsofthsm2.so -token_label Token-001 -pin 1234 -secretkey_label MYAESKEY-002 -genkey_aes24
Secret Key "MYAESKEY-002" generated.

++ ./mypkcs11test -library /usr/lib/softhsm/libsofthsm2.so -token_label Token-001 -pin 1234 -secretkey_label MYAESKEY-003 -genkey_aes16
Secret Key "MYAESKEY-003" generated.

++ ./mypkcs11test -library /usr/lib/softhsm/libsofthsm2.so -token_label Token-001 -pin 1234 -secretkey_label MYSECRETKEY-001 -importkey 0x000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f
Secret Key "MYSECRETKEY-001" imported.

++ ./mypkcs11test -library /usr/lib/softhsm/libsofthsm2.so -token_label Token-001 -pin 1234 -listkeys
Key 00 : KEYTOOL-AES
Key 01 : MYSECRETKEY-001
Key 02 : MYAESKEY-002
Key 03 : MYAESKEY-001
Key 04 : MYAESKEY-003

# ./mypkcs11test -library /usr/lib/softhsm/libsofthsm2.so -token_label Token-001 -pin 1234 -secretkey_label MYSECRETKEY-001 -hmac_sha256 'The quick brown fox jumps over the lazy dog.'
HMAC-SHA256 of "The quick brown fox jumps over the lazy dog." is 0x67a4d65b99323ae833c98f47e4a1b9dbeccc2b59557319c827e193dfeee181e8

# ./mypkcs11test -library /usr/lib/softhsm/libsofthsm2.so -token_label Token-001 -pin 1234 -secretkey_label MYAESKEY-001 -aes_cbc_pad_enc 'The quick brown fox jumps over the lazy dog.'
AES_CCB_PAD cipher text of "The quick brown fox jumps over the lazy dog." is 0x57798d669788776d49ec6f5a19394658555725cb2e60fb1eb34a3d78367c1761c81c8d6d861f66ecbf60986aed40d002

# ./mypkcs11test -library /usr/lib/softhsm/libsofthsm2.so -token_label Token-001 -pin 1234 -secretkey_label MYAESKEY-002 -aes_cbc_pad_enc 'The quick brown fox jumps over the lazy dog.'
AES_CCB_PAD cipher text of "The quick brown fox jumps over the lazy dog." is 0x3c5d94e08003091c6ff404a2333902b16aec5b09d8fc1bfdf3b5375914ad7feb19cd3752f619ab5df95fdf4c7262d5b6

# ./mypkcs11test -library /usr/lib/softhsm/libsofthsm2.so -token_label Token-001 -pin 1234 -secretkey_label MYAESKEY-003 -aes_cbc_pad_enc 'The quick brown fox jumps over the lazy dog.'
AES_CCB_PAD cipher text of "The quick brown fox jumps over the lazy dog." is 0x9411ec8fd3d2e67b93c4a81c0a78d99bd070261e046188882ef31f198990ad99aeefaa75729b87c62af6c56aa3227085

# ./mypkcs11test -library /usr/lib/softhsm/libsofthsm2.so -token_label Token-001 -pin 1234 -secretkey_label MYAESKEY-001 -delkey
Secret Key "MYAESKEY-001" deleted.

# ./mypkcs11test -library /usr/lib/softhsm/libsofthsm2.so -token_label Token-001 -pin 1234 -secretkey_label MYAESKEY-002 -delkey
Secret Key "MYAESKEY-002" deleted.

# ./mypkcs11test -library /usr/lib/softhsm/libsofthsm2.so -token_label Token-001 -pin 1234 -secretkey_label MYAESKEY-003 -delkey
Secret Key "MYAESKEY-003" deleted.

# ./mypkcs11test -library /usr/lib/softhsm/libsofthsm2.so -token_label Token-001 -pin 1234 -secretkey_label MYSECRETKEY-001 -delkey
Secret Key "MYSECRETKEY-001" deleted.

# ./mypkcs11test -library /usr/lib/softhsm/libsofthsm2.so -token_label Token-001 -pin 1234 -secretkey_label KEYTOOL-AES -hmac_sha256 'The quick brown fox jumps over the lazy dog.'
HMAC-SHA256 of "The quick brown fox jumps over the lazy dog." is 0x67a4d65b99323ae833c98f47e4a1b9dbeccc2b59557319c827e193dfeee181e8

# ./mypkcs11test -library /usr/lib/softhsm/libsofthsm2.so -token_label Token-001 -pin 1234 -secretkey_label KEYTOOL-AES -aes_cbc_pad_enc 'The quick brown fox jumps over the lazy dog.'
AES_CCB_PAD cipher text of "The quick brown fox jumps over the lazy dog." is 0xbce46469e2f7ab6b7ea767bd3252529a843ba24e890e567ef600c4a6e1051ffce0b1424e2103017de5778b49dea6d819

# ./mypkcs11test -library /usr/lib/softhsm/libsofthsm2.so -token_label Token-001 -pin 1234 -secretkey_label KEYTOOL-AES -aes_cbc_pad_enc 0x54686520717569636b2062726f776e20666f78206a756d7073206f76657220746865206c617a7920646f672e
AES_CCB_PAD cipher text of 0x54686520717569636b2062726f776e20666f78206a756d7073206f76657220746865206c617a7920646f672e is 0xbce46469e2f7ab6b7ea767bd3252529a843ba24e890e567ef600c4a6e1051ffce0b1424e2103017de5778b49dea6d819

# ./mypkcs11test -library /usr/lib/softhsm/libsofthsm2.so -token_label Token-001 -pin 1234 -secretkey_label KEYTOOL-AES -aes_cbc_pad_dec 0xbce46469e2f7ab6b7ea767bd3252529a843ba24e890e567ef600c4a6e1051ffce0b1424e2103017de5778b49dea6d819
AES_CCB_PAD clear text of 0xbce46469e2f7ab6b7ea767bd3252529a843ba24e890e567ef600c4a6e1051ffce0b1424e2103017de5778b49dea6d819 is 0x54686520717569636b2062726f776e20666f78206a756d7073206f76657220746865206c617a7920646f672e --> "The quick brown fox jumps over the lazy dog."
```

### The above is equivalent to running something like:
```
$ sudo ./examples.sh /usr/local/lib/softhsm/libsofthsm2.so Token-001 1234

or 

$ sudo ./examples.sh /usr/safenet/lunaclient/lib/libCryptoki2_64.so myvmeslot securekey

or

$ sudo ./examples.sh /opt/nfast/toolkits/pkcs11/libcknfast.so DEV_SOFTCARD securekey

```
