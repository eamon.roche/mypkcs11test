
### General Development Installation

This was done on a hardened RedHat EL 6.9 VM

### As root:
```
# yum update
# yum install git
# yum install gcc
# yum install gcc-c++
# yum install libtool
# yum install libtool-ltdl
# yum install libtool-ltdl-devel
# yum install automake
# yum install openssl
# yum install openssl-devel
#
# umask 022
#
# cd /usr/src
# wget https://storage.googleapis.com/golang/go1.8.3.linux-amd64.tar.gz
# tar -xzf go1.8.3.linux-amd64.tar.gz 
# sudo mv go /usr/local
#
# cd /usr/src
# wget https://www.openssl.org/source/openssl-1.0.2l.tar.gz
# cd openssl-1.0.2l
# ./config
# make
# make install
# mv /usr/bin/openssl /root/
# ln -s /usr/local/ssl/bin/openssl /usr/bin/openssl
#
# cd /usr/src
# git clone  https://github.com/opendnssec/SoftHSMv2.git 
# cd SoftHSMv2/
# git checkout tags/2.2.0
# sh autogen.sh 
# ./configure -with-openssl=/usr/local/opt/openssl --with-crypto-backend=openssl --enable-gost=false
# make
# make install
```
At this point SoftHSM should be ready to use.
