package main

import (
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"os"

	"strings"

	"github.com/miekg/pkcs11"
)

func printJSON(i interface{}) {
	out, err := json.Marshal(i)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s\n", out)
}

func formatInputData(isHex bool, s string) string {
	if isHex {
		return s
	}
	return "\"" + s + "\""
}

func sha1Example(p *pkcs11.Ctx, session pkcs11.SessionHandle, clearText string) {
	p.DigestInit(session, []*pkcs11.Mechanism{pkcs11.NewMechanism(pkcs11.CKM_SHA_1, nil)})
	b, isHex := getBytes(clearText)
	hash, err := p.Digest(session, b)
	if err != nil {
		panic(err)
	}
	fmt.Printf("SHA1 of %s is 0x", formatInputData(isHex, clearText))
	for _, d := range hash {
		fmt.Printf("%02x", d)
	}
	fmt.Println()
}

func generateAesKeyExample(p *pkcs11.Ctx, session pkcs11.SessionHandle, label string, size uint) pkcs11.ObjectHandle {
	if label == "" {
		fmt.Printf("Secret key label is missing.\n")
		return 0
	}
	secretKey, err := p.GenerateKey(session, []*pkcs11.Mechanism{pkcs11.NewMechanism(pkcs11.CKM_AES_KEY_GEN, nil)},
		[]*pkcs11.Attribute{
//			pkcs11.NewAttribute(pkcs11.CKA_CLASS, pkcs11.CKO_SECRET_KEY),
			pkcs11.NewAttribute(pkcs11.CKA_KEY_TYPE, pkcs11.CKK_AES),
			pkcs11.NewAttribute(pkcs11.CKA_TOKEN, true),
			pkcs11.NewAttribute(pkcs11.CKA_LABEL, label),
			pkcs11.NewAttribute(pkcs11.CKA_ENCRYPT, true),
			pkcs11.NewAttribute(pkcs11.CKA_DECRYPT, true),
			pkcs11.NewAttribute(pkcs11.CKA_SIGN, true),
			pkcs11.NewAttribute(pkcs11.CKA_VERIFY, true),
			pkcs11.NewAttribute(pkcs11.CKA_VALUE_LEN, size),
		})
	if err != nil {
		fmt.Printf("Secret key generation failed.\n")
		panic(err)
	}
	fmt.Printf("Secret Key \"%s\" generated.\n", label)
	return secretKey
}

func importSecretKeyExample(p *pkcs11.Ctx, session pkcs11.SessionHandle, label string, hs string) pkcs11.ObjectHandle {
	if label == "" {
		fmt.Printf("Secret key label is missing.\n")
		return 0
	}
	b, _ := getBytes(hs)
	secretKey, err := p.CreateObject(session,
		[]*pkcs11.Attribute{
			pkcs11.NewAttribute(pkcs11.CKA_CLASS, pkcs11.CKO_SECRET_KEY),
			pkcs11.NewAttribute(pkcs11.CKA_KEY_TYPE, pkcs11.CKK_AES),
			pkcs11.NewAttribute(pkcs11.CKA_TOKEN, true),
			pkcs11.NewAttribute(pkcs11.CKA_LABEL, label),
			pkcs11.NewAttribute(pkcs11.CKA_SIGN, true),
			pkcs11.NewAttribute(pkcs11.CKA_VERIFY, true),
			pkcs11.NewAttribute(pkcs11.CKA_VALUE, b),
		})
	if err != nil {
		fmt.Printf("Secret key import failed.\n")
		panic(err)
	}
	fmt.Printf("Secret Key \"%s\" imported.\n", label)
	return secretKey
}

func deleteSecretKey(p *pkcs11.Ctx, session pkcs11.SessionHandle, k pkcs11.ObjectHandle, label string) {
	err := p.DestroyObject(session, k)
	if err != nil {
		fmt.Printf("Failed to destroy object with label \"%s\".\n", label)
		panic(err)
	} else {
		fmt.Printf("Secret Key \"%s\" deleted.\n", label)
	}
}

func listSecretKeysExample(p *pkcs11.Ctx, session pkcs11.SessionHandle) {
	err := p.FindObjectsInit(session,
		[]*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_CLASS, pkcs11.CKO_SECRET_KEY), pkcs11.NewAttribute(pkcs11.CKA_TOKEN, true)})
	if err != nil {
		panic(err)
	}
	secretKeys, more, err := p.FindObjects(session, 50)
	if err != nil {
		panic(err)
	}
	if more {
		fmt.Printf("WARNING: More Secret keys are available than max requested.")
	}
	err = p.FindObjectsFinal(session)
	if err != nil {
		panic(err)
	}

	if len(secretKeys) == 0 {
		fmt.Printf("No secret keys exist in token.")
	} else {
		for i, k := range secretKeys {
			al, err := p.GetAttributeValue(session, k, []*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_LABEL, nil)})
			if err != nil {
				panic(err)
			}
			fmt.Printf("Key %02d : %s\n", i, al[0].Value)
		}
	}
}

func findSecretKeyByLabel(p *pkcs11.Ctx, session pkcs11.SessionHandle, label string) []pkcs11.ObjectHandle {
	err := p.FindObjectsInit(session,
		[]*pkcs11.Attribute{pkcs11.NewAttribute(pkcs11.CKA_TOKEN, true), pkcs11.NewAttribute(pkcs11.CKA_LABEL, label)})
	if err != nil {
		panic(err)
	}
	secretKeys, more, err := p.FindObjects(session, 10)
	if err != nil {
		panic(err)
	}
	if more {
		fmt.Printf("WARNING: More Secret keys available with label \"%s\".", label)
	}
	err = p.FindObjectsFinal(session)
	if err != nil {
		panic(err)
	}
	c := make([]pkcs11.ObjectHandle, len(secretKeys))
	copy(c, secretKeys)
	return c
}

func hmacSha256Example(p *pkcs11.Ctx, session pkcs11.SessionHandle, key pkcs11.ObjectHandle, clearText string) {
	err := p.SignInit(session, []*pkcs11.Mechanism{pkcs11.NewMechanism(pkcs11.CKM_SHA256_HMAC, nil)}, key)
	if err != nil {
		panic(err)
	}
	b, isHex := getBytes(clearText)
	if err != nil {
		panic(err)
	}
	hmac, err := p.Sign(session, b)
	if err != nil {
		panic(err)
	}
	fmt.Printf("HMAC-SHA256 of %s is 0x", formatInputData(isHex, clearText))
	for _, d := range hmac {
		fmt.Printf("%02x", d)
	}
	fmt.Println()
}

func aesCbcPadEncryptExample(p *pkcs11.Ctx, session pkcs11.SessionHandle, key pkcs11.ObjectHandle, clearText string) {
	iv := make([]byte, 16)
	err := p.EncryptInit(session, []*pkcs11.Mechanism{pkcs11.NewMechanism(pkcs11.CKM_AES_CBC_PAD, iv)}, key)
	if err != nil {
		panic(err)
	}
	b, isHex := getBytes(clearText)
	if err != nil {
		panic(err)
	}
	hmac, err := p.Encrypt(session, b)
	if err != nil {
		panic(err)
	}
	fmt.Printf("AES_CCB_PAD cipher text of %s is 0x", formatInputData(isHex, clearText))
	for _, d := range hmac {
		fmt.Printf("%02x", d)
	}
	fmt.Println()
}

func aesCbcPadDecryptExample(p *pkcs11.Ctx, session pkcs11.SessionHandle, key pkcs11.ObjectHandle, cipherText string) {
	iv := make([]byte, 16)
	err := p.DecryptInit(session, []*pkcs11.Mechanism{pkcs11.NewMechanism(pkcs11.CKM_AES_CBC_PAD, iv)}, key)
	if err != nil {
		panic(err)
	}
	b, isHex := getBytes(cipherText)
	if err != nil {
		panic(err)
	}
	hmac, err := p.Decrypt(session, b)
	if err != nil {
		panic(err)
	}
	fmt.Printf("AES_CCB_PAD clear text of %s is 0x", formatInputData(isHex, cipherText))
	if isHex {
		for _, d := range hmac {
			fmt.Printf("%02x", d)
		}
		fmt.Printf(" --> \"%s\"\n", string(hmac))
	}
}

func aesGcmEncryptExample(p *pkcs11.Ctx, session pkcs11.SessionHandle, key pkcs11.ObjectHandle, clearText string) {

	iv := make([]byte, 12)

	encryptGCMParams := pkcs11.NewGCMParams(iv, nil, 128)

	defer encryptGCMParams.Free()

	err := p.EncryptInit(session, []*pkcs11.Mechanism{pkcs11.NewMechanism(pkcs11.CKM_AES_GCM, encryptGCMParams)}, key)
	if err != nil {
		panic(err)
	}
	b, isHex := getBytes(clearText)
	if err != nil {
		panic(err)
	}
	ct, err := p.Encrypt(session, b)
	if err != nil {
		panic(err)
	}
	fmt.Printf("AES_GCM cipher text of %s is 0x", formatInputData(isHex, clearText))
	for _, d := range ct {
		fmt.Printf("%02x", d)
	}
	fmt.Println()
}

func aesGcmDecryptExample(p *pkcs11.Ctx, session pkcs11.SessionHandle, key pkcs11.ObjectHandle, cipherText string) {

	iv := make([]byte, 12)

	encryptGCMParams := pkcs11.NewGCMParams(iv, nil, 128)

	defer encryptGCMParams.Free()

	err := p.DecryptInit(session, []*pkcs11.Mechanism{pkcs11.NewMechanism(pkcs11.CKM_AES_GCM, encryptGCMParams)}, key)
	if err != nil {
		panic(err)
	}
	b, isHex := getBytes(cipherText)
	if err != nil {
		panic(err)
	}
	hmac, err := p.Decrypt(session, b)
	if err != nil {
		panic(err)
	}
	fmt.Printf("AES_GCM clear text of %s is 0x", formatInputData(isHex, cipherText))
	if isHex {
		for _, d := range hmac {
			fmt.Printf("%02x", d)
		}
		fmt.Printf(" --> \"%s\"\n", string(hmac))
	}
}

func getBytes(input string) ([]byte, bool) {
	var result []byte
	isHex := false
	if strings.HasPrefix(input, "0x") {
		d, err := hex.DecodeString(input[2:])
		if err != nil {
			panic(err)
		}
		result = d
		isHex = true
	} else {
		result = []byte(input)
	}
	return result, isHex
}

func main() {

	var (
		library, pin, tokenLabel, secretKeyLabel, importKey, sha1, hmacSha256, aesCbcEcrypt, aesCbcDecrypt, aesGcmEcrypt, aesGcmDecrypt string
		verbose, genKey16, genKey24, genKey32, delKey, listKeys                                      bool
	)

	flag.StringVar(&library, "library", "", "fully qualified path of the PKCS11 shared library (e.g. /usr/lib/softhsm/libsofthsm2.so)")
	flag.StringVar(&tokenLabel, "token_label", "", "token label (e.g. Token001)")
	flag.StringVar(&pin, "pin", "", "user PIN")
	flag.StringVar(&secretKeyLabel, "secretkey_label", "", "secret key label")
	flag.BoolVar(&genKey16, "genkey_aes16", false, "generate a 16 byte AES key with label <secretkey_label>")
	flag.BoolVar(&genKey24, "genkey_aes24", false, "generate a 24 byte AES key with label <secretkey_label>")
	flag.BoolVar(&genKey32, "genkey_aes32", false, "generate a 32 byte AES key with label <secretkey_label>")
	flag.StringVar(&importKey, "importkey", "", "import a generic secret key HexString encoded with label <secretkey_label>")
	flag.BoolVar(&delKey, "delkey", false, "delete secret key with <secretkey_label>")
	flag.BoolVar(&listKeys, "listkeys", false, "list all secret keys")
	flag.StringVar(&sha1, "sha1", "", "SHA1 hash of <string>")
	flag.StringVar(&hmacSha256, "hmac_sha256", "", "HMAC-SHA256 of <string> using secret key with label <secretkey_label>")
	flag.StringVar(&aesCbcEcrypt, "aes_cbc_pad_enc", "", "AES_CBC_PAD encryption of <string> using key with label <secretkey_label>")
	flag.StringVar(&aesCbcDecrypt, "aes_cbc_pad_dec", "", "AES_CBC_PAD decryption of <string> using key with label <secretkey_label>")
	flag.StringVar(&aesGcmEcrypt, "aes_gcm_enc", "", "AES_GCM encryption of <string> using key with label <secretkey_label>")
	flag.StringVar(&aesGcmDecrypt, "aes_gcm_dec", "", "AES_GCM decryption of <string> using key with label <secretkey_label>")
	flag.BoolVar(&verbose, "verbose", false, "verbose output")
	flag.Parse()

	// Do some flag validations
	if library == "" || tokenLabel == "" || pin == "" {
		fmt.Println("Flags -library, -token_label, and -pin are always mandatory!")
		os.Exit(-1)
	}
	if (genKey16 || genKey24 || genKey32 || delKey || hmacSha256 != "" || importKey != "") && secretKeyLabel == "" {
		fmt.Println("Missing flag -secretkey_label!")
		os.Exit(-1)
	}
	if importKey != "" && (len(importKey) != 66 || !strings.HasPrefix(importKey, "0x")) {
		fmt.Println("Invalid Secret key data - must be 32 bytes and be in the form 0x...")
		os.Exit(-1)
	}

	// Initialize the library
	p := pkcs11.New(library)
	err := p.Initialize()
	if err != nil {
		panic(err)
	}

	defer p.Destroy()
	defer p.Finalize()

	slots, err := p.GetSlotList(true)
	if err != nil {
		panic(err)
	}

	var (
		slot      uint
		slotFound bool
	)

	for i, s := range slots {
		ti, err := p.GetTokenInfo(s)
		if err != nil {
			panic(err)
		}
		si, err := p.GetSlotInfo(s)
		if err != nil {
			panic(err)
		}
		m, err := p.GetMechanismList(s)
		if err != nil {
			panic(err)
		}
		if ti.Label != tokenLabel {
			continue
		} else {
			if verbose {
				fmt.Printf("Slot : %d\n", i)
				fmt.Print("\tSlot Info  : ")
				printJSON(si)
				fmt.Print("\tToken Info : ")
				printJSON(ti)
				fmt.Print("\tMechanisms : ")
				printJSON(m)
			}
			slot = s
			slotFound = true
		}
	}
	if !slotFound {
		fmt.Printf("Token \"%s\" not found\n", tokenLabel)
		os.Exit(-1)
	}

	session, err := p.OpenSession(slot, pkcs11.CKF_SERIAL_SESSION|pkcs11.CKF_RW_SESSION)
	if err != nil {
		panic(err)
	}
	defer p.CloseSession(session)

	err = p.Login(session, pkcs11.CKU_USER, pin)
	if err != nil {
		panic(err)
	}
	defer p.Logout(session)

	if secretKeyLabel != "" && (genKey16 || genKey24 || genKey32 || delKey || hmacSha256 != "" || importKey != "" || aesCbcEcrypt != "" || aesCbcDecrypt != "" || aesGcmEcrypt != "" || aesGcmDecrypt != "") {
		keys := findSecretKeyByLabel(p, session, secretKeyLabel)
		l := len(keys)
		if l > 1 {
			fmt.Printf("WARNING!!: There are %d Secret key with label \"%s\".\n", l, secretKeyLabel)
		} else if l == 0 && !genKey16 && !genKey24 && !genKey32 && importKey == "" {
			fmt.Printf("Secret key with label \"%s\" NOT found.\n", secretKeyLabel)
			os.Exit(-1)
		}
		if genKey16 || genKey24 || genKey32 {
			if l == 0 {
				if genKey16 {
					generateAesKeyExample(p, session, secretKeyLabel, 16)
				} else if genKey24 {
					generateAesKeyExample(p, session, secretKeyLabel, 24)
				} else {
					generateAesKeyExample(p, session, secretKeyLabel, 32)
				}
			} else {
				fmt.Printf("Secret Key \"%s\" already exists.\n", secretKeyLabel)
			}
		} else if delKey {
			if l > 0 {
				if l > 1 {
					fmt.Printf("Attempting to delete the first key with label \"%s\"...\n", secretKeyLabel)
				}
				deleteSecretKey(p, session, keys[0], secretKeyLabel)
			} else {
				fmt.Printf("Secret key with label \"%s\" does not exist.\n", secretKeyLabel)
			}
		} else if hmacSha256 != "" {
			if l == 1 {
				hmacSha256Example(p, session, keys[0], hmacSha256)
			}
		} else if aesCbcEcrypt != "" {
			if l == 1 {
				aesCbcPadEncryptExample(p, session, keys[0], aesCbcEcrypt)
			}
		} else if aesCbcDecrypt != "" {
			if l == 1 {
				aesCbcPadDecryptExample(p, session, keys[0], aesCbcDecrypt)
			}
		} else if aesGcmEcrypt != "" {
			if l == 1 {
				aesGcmEncryptExample(p, session, keys[0], aesGcmEcrypt)
			}
		} else if aesGcmDecrypt != "" {
			if l == 1 {
				aesGcmDecryptExample(p, session, keys[0], aesGcmDecrypt)
			}
		} else if importKey != "" {
			if l == 0 {
				importSecretKeyExample(p, session, secretKeyLabel, importKey)
			} else {
				fmt.Printf("Secret Key \"%s\" already exists.\n", secretKeyLabel)
			}
		} else {
			fmt.Println("Nothing to do!")
		}
	} else if sha1 != "" {
		sha1Example(p, session, sha1)
	} else if listKeys {
		listSecretKeysExample(p, session)
	} else {
		fmt.Println("Nothing to do!")
	}
	fmt.Println()
}
